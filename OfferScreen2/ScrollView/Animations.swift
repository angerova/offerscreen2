//
//  Animations.swift
//  OfferScreen2
//
//  Created by Алина Ангерова on 18/03/2019.
//  Copyright © 2019 Алина Ангерова. All rights reserved.
//

import Foundation
import UIKit


func createPusleAnimationGroups()-> CAAnimationGroup{
    let pulse = CASpringAnimation(keyPath: "transform.scale")
    pulse.duration = 0.3
    pulse.fromValue = 1.0
    pulse.toValue = 1.02
    pulse.autoreverses = true
    pulse.initialVelocity = 0.6
    pulse.damping = 9.0
    let pulseAnimationGroup = CAAnimationGroup()
    pulseAnimationGroup.duration = 2.5
    pulseAnimationGroup.repeatCount = .greatestFiniteMagnitude
    pulseAnimationGroup.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
    pulseAnimationGroup.animations = [pulse]
    return pulseAnimationGroup
}

func createPuslatingAnimationGroups()-> CAAnimationGroup{
    
    let scaleAnimationX = CABasicAnimation(keyPath: "transform.scale.x")
    scaleAnimationX.fromValue = 1.0
    scaleAnimationX.toValue = 1.09
    scaleAnimationX.duration = 0.9
    
    let scaleAnimationY = CABasicAnimation(keyPath: "transform.scale.y")
    scaleAnimationY.fromValue = 1.0
    scaleAnimationY.toValue = 1.5
    scaleAnimationY.duration = 0.9
    
    let opacityAnimation = CABasicAnimation(keyPath: #keyPath(CALayer.opacity))
    opacityAnimation.fromValue = 1.0
    opacityAnimation.toValue = 0.0
    opacityAnimation.duration = 0.9
    
    let pulsatingAnimationGroup = CAAnimationGroup()
    pulsatingAnimationGroup.animations = [scaleAnimationX, scaleAnimationY, opacityAnimation]
    pulsatingAnimationGroup.duration = 2.5
    pulsatingAnimationGroup.repeatCount = .greatestFiniteMagnitude
    pulsatingAnimationGroup.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
    return pulsatingAnimationGroup
}
