

import UIKit

class ScrollContentView: UIView {
   

    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var try7DaysForFree: UIButton!
    @IBOutlet weak var monthlyButton: UIButton!
    @IBOutlet weak var termsOfServiceButton: UIButton!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var restorePurchaseButton: UIButton!
    
    let cornerRadius: CGFloat = 28.5
    var contentViewHeight: CGFloat = 810
    
    var pulsatingArray = [CAShapeLayer]()
    
    @IBAction func showWebView(_ sender: Any) {
        
    }
    
    @IBAction func restorePurchases(_ sender: Any) {
        

    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("ScrollContentView", owner: self, options: nil)
        addSubview(contentView)
        contentView.backgroundColor = .clear
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    
        setAttributedStringForButton(title: "Terms of Service and Privacy Policy", size: 13, button: termsOfServiceButton)
        headerImageView.image = UIImage(named: "slim_girl_offer")
        try7DaysForFree.layer.masksToBounds = false
        try7DaysForFree.layer.cornerRadius = cornerRadius
        monthlyButton.layer.masksToBounds = false
        monthlyButton.layer.cornerRadius = cornerRadius
        monthlyButton.layer.borderWidth = 1
        monthlyButton.layer.borderColor = #colorLiteral(red: 0.262745098, green: 0.5411764706, blue: 0.4549019608, alpha: 1)
        contentView.backgroundColor = #colorLiteral(red: 0.9505085349, green: 0.9734930396, blue: 0.9466121793, alpha: 1)
        try7DaysForFree.isUserInteractionEnabled = true
        
 
    }
 
    
    
    func setAttributedStringForButton(title: String, size: CGFloat, button: UIButton){
        let attrs = [NSAttributedString.Key.font : UIFont(name: "HelveticaNeue", size: size)!, NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.5098039216, green: 0.5098039216, blue: 0.5098039216, alpha: 1), NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
        let attributedString = NSMutableAttributedString(string:"")
        let buttonTitleStr = NSMutableAttributedString(string: title, attributes: attrs)
        attributedString.append(buttonTitleStr)
        button.setAttributedTitle(attributedString, for: .normal)
    }
    
    func createPulsatingLayer(){
        
        for _ in 0...1 {

            let circularPath2 = UIBezierPath            (roundedRect: CGRect(x: -170.5, y: -27.5, width: 341, height: 55), cornerRadius: 27.5)
        
            let pulsatingLayer = CAShapeLayer()
            pulsatingLayer.path = circularPath2.cgPath
            pulsatingLayer.lineWidth = 1.0
            pulsatingLayer.fillColor = UIColor.clear.cgColor
            pulsatingLayer.lineCap = CAShapeLayerLineCap.round
            pulsatingLayer.position = CGPoint(x: try7DaysForFree.frame.size.width / 2.0, y: try7DaysForFree.frame.size.height / 2.0)
            try7DaysForFree.layer.addSublayer(pulsatingLayer)
            pulsatingLayer.strokeColor = #colorLiteral(red: 0.2342539366, green: 0.482681864, blue: 0.4092106641, alpha: 1)
            pulsatingArray.append(pulsatingLayer)
        }
    }
    
    
    func addPulseAnimation(){
        let pulseAnimationGroup = createPusleAnimationGroups()
        let pulsatingAnimationGroup = createPuslatingAnimationGroups()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: {

            self.try7DaysForFree.layer.add(pulseAnimationGroup, forKey: "groupanimation")
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4, execute: {
                self.pulsatingArray[0].add(pulsatingAnimationGroup, forKey: "groupanimation")
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                    self.pulsatingArray[1].add(pulsatingAnimationGroup, forKey: "groupanimation")
                })
            })
        })
        
    }
    
    func removePulsatingLayers(){
        if pulsatingArray.count == 0{
            return
        }
        removePulseAnimation()
        pulsatingArray[0].removeFromSuperlayer()
        pulsatingArray[1].removeFromSuperlayer()
        pulsatingArray.removeAll()
    }

    
    func removePulseAnimation(){
        try7DaysForFree.layer.removeAllAnimations()
        pulsatingArray[0].removeAllAnimations()
        pulsatingArray[1].removeAllAnimations()
        
    }
    
    func resetAnimations(){
        removePulsatingLayers()
        createPulsatingLayer()
        addPulseAnimation()
    }
    
}
