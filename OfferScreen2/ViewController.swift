

import UIKit

class ViewController: UIViewController{
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var contentView: ScrollContentView!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        scrollViewHeightConstant.constant = contentView.contentViewHeight - self.view.bounds.height
        createNotificationCenter()
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    deinit{
        NotificationCenter.default.removeObserver(self)
    }
    
    func createNotificationCenter(){
        NotificationCenter.default.addObserver(
            self,selector: #selector(cameBackFromSleep),
            name: UIApplication.didBecomeActiveNotification,
            object: nil
        )
    }
    
    
    
    @objc func cameBackFromSleep(sender : AnyObject) {
        contentView.resetAnimations()
    }
   
}

